﻿Shader "Martinelli/NeonProjection"
{
    Properties
    {
        [NoScaleOffset] _MainTex ("Texture", 2D) = "white" {}
        _Speed ("Speed",Range(1,50)) = 0
         _OutlineThickness ("Outline Thickness", Range(0,1)) = 0.1
         _Tess ("Tessellation", Range(1,32)) = 4
    }
    SubShader
    {
        Pass
        {
            Tags {"LightMode"="ForwardBase" "Queue"= "Transparent"	 }

		LOD 100
		ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag 
            #include "UnityCG.cginc" // for UnityObjectToWorldNormal
            #include "UnityLightingCommon.cginc" // for _LightColor0

            struct v2f
            {
                float2 uv : TEXCOORD0;
                fixed4 diff : COLOR0; // diffuse lighting color
                float3 normalDir : TEXCOORD1;
      	     	float3 viewDir : TEXCOORD3;
                float4 vertex : SV_POSITION;
            };
             sampler2D _MainTex;
            float _Speed;
            float _OutlineThickness;
             float _Tess;


         
            v2f vert (appdata_base v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;
                // get vertex normal in world space
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                // dot product between normal and light direction for
                // standard diffuse (Lambert) lighting
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                // factor in the light color
                o.diff = nl * _LightColor0;
               	o.vertex.x+=sin(o.vertex.y*_Time.w/15%0.5)/20;
               	o.vertex.z+=sin(_Time.w/15%0.5)/50;

                float4 posWorld = mul(unity_ObjectToWorld, v.vertex);
                	o.normalDir = normalize ( mul( float4( v.normal, 0.0 ), unity_WorldToObject).xyz );
                	o.viewDir = normalize( _WorldSpaceCameraPos.xyz - posWorld.xyz ); //vector from object to the camera
                return o;
            }
            
           

            fixed4 frag (v2f i) : SV_Target
            {
                // sample texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // multiply by lighting
                 float outlineStrength = saturate( (dot(i.normalDir, i.viewDir ) - _OutlineThickness) * 1000 );

                col *= i.diff;
              	col.r += 0.3f;
                col.g =0 ;
                col.b +=0.1f;
                col.a += 0.8- outlineStrength;
                float x = (col.a>=1)?255:0;
              	  col.a*= ((i.vertex.y+_Time.w*_Speed)%6>2)?1:0.5f;
                col.r+=x;
                col.b+=x;

                return col;

               
            }
            ENDCG
        }
    }
}
